package service

// Server is a contract
type Server interface {
	Run(port int) error
}
