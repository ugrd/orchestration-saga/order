package service

import (
	"gitlab.com/ugrd/orchestration-saga/booking/config"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
)

// Option is a function
type Option func(*GRPCService)

// WithConfig is a function option
func WithConfig(conf *config.Config) Option {
	return func(service *GRPCService) {
		service.config = conf
	}
}

// WithRepo is a function option
func WithRepo(repo *registry.Repositories) Option {
	return func(service *GRPCService) {
		service.repo = repo
	}
}

// WithJWT is a function option
func WithJWT(j *jwt.JWT) Option {
	return func(service *GRPCService) {
		service.jwt = j
	}
}

// WithGRPCClient is a function
func WithGRPCClient(client *rpc.RPC) Option {
	return func(service *GRPCService) {
		service.RPC = client
	}
}
