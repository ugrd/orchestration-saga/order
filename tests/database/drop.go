package database

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gorm.io/gorm"
)

// Drop is struct to hold connection to DB
type Drop struct {
	db *gorm.DB
}

// NewDrop is constructor
func NewDrop(db *gorm.DB) *Drop {
	return &Drop{db: db}
}

// DropPostgresql is function drop all tables of postgresql
func (op *Drop) DropPostgresql(ctx context.Context) error {
	for _, table := range registry.CollectTables() {
		ok := op.db.WithContext(ctx).Migrator().HasTable(table.Name)
		if ok {
			err := op.db.WithContext(ctx).Migrator().DropTable(table.Name)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Reset is function drop all tables & recreate them
func (op *Drop) Reset(ctx context.Context) error {
	err := op.DropPostgresql(ctx)
	if err != nil {
		return err
	}

	err = registry.NewRegistry().AutoMigrate(op.db)
	if err != nil {
		return err
	}

	return nil
}
