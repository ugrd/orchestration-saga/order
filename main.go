package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
	"gitlab.com/ugrd/orchestration-saga/booking/config"
	"gitlab.com/ugrd/orchestration-saga/booking/grpc/service"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/core"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/core/provider/connection"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/booking/interface/cmd"
	"gitlab.com/ugrd/orchestration-saga/package/configurator"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc/client"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc/interceptors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"os"
)

func main() {
	if errEnv := godotenv.Load(); errEnv != nil {
		log.Fatal("Error loading .env file")
	}

	conf := config.New()

	db, errConn := connection.NewDBConnection(conf)
	if errConn != nil {
		log.Fatalf("unable connect to database, %v", errConn)
	}

	jwt, errJWT := core.NewJWT(conf)
	if errJWT != nil {
		log.Fatalf("unable to initialize JWT, err: %v", errJWT)
	}

	repo := registry.NewRepo(db)

	command := cmd.NewCommand(
		cmd.WithConfig(conf),
		cmd.WithRepo(repo),
	)

	cfg := configurator.New()

	// initialize rpc connection to account service
	rpcAccount, err := grpc.Dial(fmt.Sprintf("%s:%d", "localhost", cfg.Account.RPCPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			interceptors.UnaryAuthClientInterceptor(),
		),
	)
	if err != nil {
		log.Fatalf("unable to connect to account service: %v", err)
	}

	// initialize rpc connection to catalog service
	rpcCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", "localhost", cfg.Catalog.RPCPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			interceptors.UnaryAuthClientInterceptor(),
		),
	)
	if err != nil {
		log.Fatalf("unable to connect to catalog service: %v", err)
	}

	// initialize rpc connection to payment service
	rpcPayment, err := grpc.Dial(fmt.Sprintf("%s:%d", "localhost", cfg.Payment.RPCPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			interceptors.UnaryAuthClientInterceptor(),
		),
	)
	if err != nil {
		log.Fatalf("unable to connect to payment service: %v", err)
	}

	// initialize rpc connection to seats service
	rpcSeats, err := grpc.Dial(fmt.Sprintf("%s:%d", "localhost", cfg.Seats.RPCPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			interceptors.UnaryAuthClientInterceptor(),
		),
	)
	if err != nil {
		log.Fatalf("unable to connect to seats service: %v", err)
	}

	// initialize grpc client
	rpcClient := rpc.NewRPC(client.NewGrpcServiceClient(
		client.WithAccountClientService(rpcAccount),
		client.WithCatalogClientService(rpcCatalog),
		client.WithPaymentClientService(rpcPayment),
		client.WithSeatsClientService(rpcSeats),
	))

	app := cmd.NewCLI()
	app.Commands = command.Build()

	app.Action = func(ctx *cli.Context) error {
		serv := service.NewGRPCService(
			service.WithConfig(conf),
			service.WithRepo(repo),
			service.WithJWT(jwt),
			service.WithGRPCClient(rpcClient),
		)

		err = serv.Run(cfg.Booking.RPCPort)
		if err != nil {
			return err
		}

		return nil
	}

	err = app.Run(os.Args)
	if err != nil {
		log.Fatalf("Unable to run CLI command, err: %v", err)
	}
}
