package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/pkg/common"
)

// OrderRepoInterface is order repo contract
type OrderRepoInterface interface {
	Create(ctx context.Context, order *entity.Order) error
	Update(ctx context.Context, order *entity.Order, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Order, error)
	FindByToken(ctx context.Context, code string) (*entity.Order, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Order, error)
}
