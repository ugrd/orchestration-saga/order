package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/pkg/common"
)

// SagaStepRepoInterface is saga step repo contract
type SagaStepRepoInterface interface {
	Create(ctx context.Context, step *entity.SagaStep) error
	Update(ctx context.Context, step *entity.SagaStep, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.SagaStep, error)
	FindByOrderID(ctx context.Context, orderID uint) (*entity.SagaStep, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.SagaStep, error)
}
