package entity

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"time"
)

// SagaStep is a struct
type SagaStep struct {
	ID        uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	OrderID   uint      `gorm:"column:transport_id;not null;index" json:"transport_id"`
	Service   string    `gorm:"column:service;size:100;not null" json:"service"`
	Action    string    `gorm:"column:action;size:100;not null" json:"action"`
	Status    string    `gorm:"column:status;size:100;not null" json:"status"`
	Reason    string    `gorm:"column:reason;size:255;not null;default:''" json:"reason"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// TableName is a function return table name
func (u *SagaStep) TableName() string {
	return "saga_steps"
}

// FilterableFields is a function return filterable fields
func (u *SagaStep) FilterableFields() []interface{} {
	return []interface{}{"id", "transaction_id"}
}

// TimeFields is a function return time fields
func (u *SagaStep) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at"}
}

// Implements base entity methods
var _ contract.EntityInterface = &SagaStep{}
