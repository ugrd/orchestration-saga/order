package entity

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Order is a struct that represents orders table
type Order struct {
	ID           uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	TransportID  uint           `gorm:"column:transport_id;not null;index" json:"transport_id"`
	OrderToken   string         `gorm:"column:order_token;size:100;not null" json:"order_token"`
	UserID       uint           `gorm:"column:user_id;not null;index" json:"user_id"`
	Total        uint64         `gorm:"column:total;not null" json:"total"`
	Status       string         `gorm:"column:status;size:100;not null" json:"status"`
	CreatedAt    time.Time      `json:"created_at"`
	UpdatedAt    time.Time      `json:"updated_at"`
	DeletedAt    gorm.DeletedAt `json:"deleted_at"`
	OrderDetails []*OrderDetail `json:"order_details"`
}

const (
	// StatusPending is a constant
	StatusPending = "PENDING"
	// StatusFailed is a constant
	StatusFailed = "FAILED"
	// StatusCompleted is a constant
	StatusCompleted = "COMPLETED"
)

// Implements base entity methods
var _ contract.EntityInterface = &Order{}

// TableName is a function return table name
func (u *Order) TableName() string {
	return "orders"
}

// FilterableFields is a function return filterable fields
func (u *Order) FilterableFields() []interface{} {
	return []interface{}{"id", "transport_id", "order_token", "user_id"}
}

// TimeFields is a function return time fields
func (u *Order) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
