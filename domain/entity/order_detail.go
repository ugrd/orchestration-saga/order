package entity

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"time"
)

// OrderDetail is a struct that represents order_details table
type OrderDetail struct {
	ID          uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	OrderID     uint      `gorm:"column:order_id;not null;index" json:"order_id"`
	TicketID    uint      `gorm:"column:ticket_id;not null;index" json:"ticket_id"`
	PassengerID uint      `gorm:"column:passenger_id;not null;index" json:"passenger_id"`
	Price       uint64    `gorm:"column:price;not null" json:"price"`
	Quantity    uint32    `gorm:"column:quantity;not null" json:"quantity"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Order       Order     `json:"order"`
}

// Implements base entity methods
var _ contract.EntityInterface = &OrderDetail{}

// TableName is a function return table name
func (u *OrderDetail) TableName() string {
	return "order_details"
}

// FilterableFields is a function return filterable fields
func (u *OrderDetail) FilterableFields() []interface{} {
	return []interface{}{"id", "order_id", "ticket_id", "passenger_id"}
}

// TimeFields is a function return time fields
func (u *OrderDetail) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at"}
}
