package entity

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"gorm.io/datatypes"
	"time"
)

// SagaCompensationStep is a struct
type SagaCompensationStep struct {
	ID               uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	SagaStepID       uint           `gorm:"column:saga_step_id;not null;index" json:"saga_step_id"`
	CompensationType string         `gorm:"column:compensation_type;size:100;not null" json:"compensation_type"`
	Data             datatypes.JSON `gorm:"column:compensation_type" json:"data"`
	CreatedAt        time.Time      `json:"created_at"`
	UpdatedAt        time.Time      `json:"updated_at"`
}

// TableName is a function return table name
func (u *SagaCompensationStep) TableName() string {
	return "saga_compensation_steps"
}

// FilterableFields is a function return filterable fields
func (u *SagaCompensationStep) FilterableFields() []interface{} {
	return []interface{}{"id", "saga_step_id", "compensation_type"}
}

// TimeFields is a function return time fields
func (u *SagaCompensationStep) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at"}
}

// Implements base entity methods
var _ contract.EntityInterface = &SagaCompensationStep{}
