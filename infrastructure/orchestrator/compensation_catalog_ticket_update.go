package orchestrator

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
)

// CompensationCatalogTicketUpdate is a struct
type CompensationCatalogTicketUpdate struct {
	dep *dependency.Dependency
}

// Execute is a method
func (cctu *CompensationCatalogTicketUpdate) Execute(ctx context.Context, param *CompensationPayload) error {
	logger := log.New()
	logger.Info("[Compensation: CompensationCatalogTicketUpdate] starting....")
	logger.Info("[Compensation: CompensationCatalogTicketUpdate] args: %v", param)

	for _, r := range param.Tickets {
		_, err := cctu.dep.RPC.IncreaseTicketQty(ctx, &catalog.IncreaseTicketQtyRequest{
			Id:  r.ID,
			Qty: uint32(r.Qty),
		})

		if err != nil {
			logger.Warningf("[Compensation: CompensationCatalogTicketUpdate], error :%v", err)
			return err
		}
	}

	return nil
}

// NewCompensationCatalogTicketUpdate is a constructor
func NewCompensationCatalogTicketUpdate(dep *dependency.Dependency) CompensationInterface {
	return &CompensationCatalogTicketUpdate{dep: dep}
}

var _ CompensationInterface = &CompensationCatalogTicketUpdate{}
