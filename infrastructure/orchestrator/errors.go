package orchestrator

import "errors"

var (
	// ErrNotEnoughBalance is an error
	ErrNotEnoughBalance = errors.New("orchestrator.payment.not_enough_balance")
	// ErrUnauthenticated is an error
	ErrUnauthenticated = errors.New("orchestrator.account.unauthenticated")
	// ErrUnknown is an error
	ErrUnknown = errors.New("orchestrator.unknown")
	// ErrInvalidTransport is an error
	ErrInvalidTransport = errors.New("orchestrator.catalog.invalid_transport")
)
