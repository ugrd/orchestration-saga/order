package orchestrator

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
)

// attemptSeatsError is a struct
type attemptSeatsError struct {
	id    uint64
	qty   int
	error error
}

// StepSeatsUpdate is a struct
type StepSeatsUpdate struct {
	order         *entity.Order
	dep           *dependency.Dependency
	compensations []CompensationInterface
}

// SetOrder is a method
func (ssu *StepSeatsUpdate) SetOrder(order *entity.Order) StepInterface {
	ssu.order = order

	return ssu
}

// Execute is a method
func (ssu *StepSeatsUpdate) Execute(ctx context.Context, req *OrderPayload) (StepResult, error) {
	logger := log.New()

	logger.Info("[Step: StepSeatsUpdate] starting...")

	var attempts []*attemptSeatsError

	for i, v := range req.OrderDetails {
		_, err := ssu.dep.RPC.CreateSeatsUpdate(ctx, &seats.CreateSeatsUpdateRequest{
			TransportId:   req.TransportID,
			TransactionId: fmt.Sprintf("%s%d", req.TransactionID, i),
			TicketId:      v.TicketID,
		})

		if err != nil {
			logger.Warningf("[Step: StepSeatsUpdate] error request update seats: %v", err)
			attempts = append(attempts, &attemptSeatsError{
				id:    v.TicketID,
				qty:   int(v.Quantity),
				error: err,
			})
		}
	}

	if len(attempts) > 0 {
		// Start compensation
		compPayload := CompensationPayload{
			TransportID:   req.TransportID,
			TransactionID: req.TransactionID,
			UserID:        req.UserID,
			Amount:        ssu.order.Total,
		}

		for _, att := range attempts {
			compPayload.Tickets = append(compPayload.Tickets, struct {
				ID  uint64
				Qty int
			}{ID: att.id, Qty: att.qty})
		}

		err := ssu.compensate(ctx, &compPayload)
		if err != nil {
			logger.Warningf("[Step: StepSeatsUpdate] error request compensation seats: %v", err)
			return nil, ErrUnknown
		}

		return nil, ErrUnknown
	}

	return nil, nil
}

// compensate is a method
func (ssu *StepSeatsUpdate) compensate(ctx context.Context, payload *CompensationPayload) error {
	for _, compensation := range ssu.compensations {
		err := compensation.Execute(ctx, payload)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewStepSeatsUpdate is a constructor
func NewStepSeatsUpdate(dep *dependency.Dependency, compensations ...CompensationInterface) StepInterface {
	return &StepSeatsUpdate{
		dep:           dep,
		compensations: compensations,
	}
}

var _ StepInterface = &StepSeatsUpdate{}
