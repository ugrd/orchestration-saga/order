package orchestrator

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
)

// attemptTicketError is a struct
type attemptTicketError struct {
	id    uint64
	qty   int
	error error
}

// StepCatalogTicketUpdate is a struct
type StepCatalogTicketUpdate struct {
	order         *entity.Order
	dep           *dependency.Dependency
	compensations []CompensationInterface
}

// SetOrder is a method
func (sct *StepCatalogTicketUpdate) SetOrder(order *entity.Order) StepInterface {
	sct.order = order

	return sct
}

// Execute is a method
func (sct *StepCatalogTicketUpdate) Execute(ctx context.Context, req *OrderPayload) (StepResult, error) {
	logger := log.New()

	logger.Info("[Step: StepCatalogTicketUpdate] starting...")

	var amount uint64
	var attempts []*attemptTicketError

	for _, r := range req.OrderDetails {
		resp, err := sct.dep.RPC.DecreaseTicketQty(ctx, &catalog.DecreaseTicketQtyRequest{
			Id:  r.TicketID,
			Qty: r.Quantity,
		})

		if err != nil {
			logger.Warningf("[Step: StepCatalogTicketUpdate] error request update ticket: %v", err)
			attempts = append(attempts, &attemptTicketError{
				id:    r.TicketID,
				qty:   int(r.Quantity),
				error: err,
			})
		}

		if resp != nil {
			amount = amount + (resp.Price * uint64(r.Quantity))
		}
	}

	if len(attempts) > 0 {
		// Start compensation
		compPayload := CompensationPayload{
			TransportID:   req.TransportID,
			TransactionID: req.TransactionID,
			UserID:        req.UserID,
			Amount:        amount,
		}

		for _, att := range attempts {
			compPayload.Tickets = append(compPayload.Tickets, struct {
				ID  uint64
				Qty int
			}{ID: att.id, Qty: att.qty})
		}

		err := sct.compensate(ctx, &compPayload)
		if err != nil {
			logger.Warningf("[Step: StepCatalogTicketUpdate] error request compensation ticket: %v", err)
			return nil, ErrUnknown
		}

		return nil, ErrUnknown
	}

	sct.order.Total = amount

	_ = sct.dep.Repository.OrderRepo.Update(ctx, &entity.Order{
		Total: amount,
	}, sct.order.ID)

	return nil, nil
}

// Compensate is a method
func (sct *StepCatalogTicketUpdate) compensate(ctx context.Context, payload *CompensationPayload) error {
	for _, compensation := range sct.compensations {
		err := compensation.Execute(ctx, payload)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewStepCatalogTicketUpdate is a constructor
func NewStepCatalogTicketUpdate(dep *dependency.Dependency, compensations ...CompensationInterface) StepInterface {
	return &StepCatalogTicketUpdate{
		dep:           dep,
		compensations: compensations,
	}
}

var _ StepInterface = &StepCatalogTicketUpdate{}
