package orchestrator

import "context"

type CompensationPayload struct {
	TransactionID string
	TransportID   uint64
	UserID        uint64
	Amount        uint64
	Tickets       []struct {
		ID  uint64
		Qty int
	}
}

type CompensationInterface interface {
	Execute(ctx context.Context, param *CompensationPayload) error
}
