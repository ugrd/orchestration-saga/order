package orchestrator

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	"gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"strconv"
)

// OrderPayload is a struct
type OrderPayload struct {
	TransactionID string         `json:"transaction_id"`
	UserID        uint64         `json:"user_id"`
	TransportID   uint64         `json:"transport_id"`
	OrderDetails  []*OrderDetail `json:"order_details"`
}

// OrderDetail is a struct
type OrderDetail struct {
	TicketID    uint64 `json:"ticket_id"`
	PassengerID uint64 `json:"passenger_id"`
	Quantity    uint32 `json:"quantity"`
}

// User is a struct
type User struct {
	ID       uint64
	Name     string
	Username string
}

//type OrchestrationStep map[]

// Orchestrator is a struct
type Orchestrator struct {
	User   *User
	Order  *entity.Order
	Steps  []StepInterface
	Dep    *dependency.Dependency
	logger *logrus.Logger
}

// New is a constructor
func New(dep *dependency.Dependency) *Orchestrator {
	return &Orchestrator{
		Dep:    dep,
		logger: logger.New(),
	}
}

// SetOrder is a method
func (orc *Orchestrator) SetOrder(order *entity.Order) *Orchestrator {
	orc.Order = order

	return orc
}

// SetSteps is a method to set steps
func (orc *Orchestrator) SetSteps(steps ...StepInterface) *Orchestrator {
	orc.Steps = steps

	return orc
}

// Prepare is a method to prepare the transaction
func (orc *Orchestrator) Prepare(ctx context.Context, transportID uint64) (*Orchestrator, error) {
	id := getUserIDFromContext(ctx)

	orc.logger.Infof("[orchestrator] preparing.... userID: %d, TransportID: %d", id, transportID)

	r, err := orc.Dep.RPC.FindUser(ctx, &account.FindUserRequest{
		Id: id,
	})

	if err != nil {
		orc.logger.Warnf("[orchestrator] error finding user :%v", err)
		return nil, ErrUnauthenticated
	}

	_, err = orc.Dep.RPC.GetTransportByID(ctx, &catalog.TransportIdentifierRequest{
		Id: transportID,
	})

	if err != nil {
		orc.logger.Warnf("[orchestrator] error finding transport :%v", err)
		return nil, ErrInvalidTransport
	}

	orc.logger.Info("preparing done..")

	orc.User = &User{
		ID:       r.Id,
		Name:     r.Username,
		Username: r.Username,
	}

	return orc, nil
}

// Handle is a method
func (orc *Orchestrator) Handle(ctx context.Context, payload *OrderPayload) error {
	orc.logger.Infof("[orchestrator] number steps: %d", len(orc.Steps))
	for i, step := range orc.Steps {
		step.SetOrder(orc.Order)
		orc.logger.Infof("[orchestrator] handling step.... IDX: %d", i)

		_, err := step.Execute(ctx, payload)
		if err != nil {
			orc.logger.Warnf("[orchestrator] error handling step.... IDX: %d, err:%v", i, err)
			return err
		}
	}

	_ = orc.Dep.Repository.OrderRepo.Update(ctx, &entity.Order{
		Status: entity.StatusCompleted,
	}, orc.Order.ID)

	return nil
}

func getUserIDFromContext(ctx context.Context) uint64 {
	value := ctx.Value(entity.XID).(string)
	result, _ := strconv.ParseUint(value, 10, 64)

	return result
}
