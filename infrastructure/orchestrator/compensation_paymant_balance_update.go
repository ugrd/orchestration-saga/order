package orchestrator

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
)

// CompensationPaymentBalanceUpdate is a struct
type CompensationPaymentBalanceUpdate struct {
	dep *dependency.Dependency
}

// Execute is a method
func (cpbu *CompensationPaymentBalanceUpdate) Execute(ctx context.Context, param *CompensationPayload) error {
	logger := log.New()
	logger.Info("[Compensation: CompensationPaymentBalanceUpdate] starting...")
	logger.Info("[Compensation: CompensationPaymentBalanceUpdate] args: %v", param)

	_, err := cpbu.dep.RPC.CancelTransaction(ctx, &payment.CancelTransactionRequest{
		TransactionId: param.TransactionID,
		UserId:        param.UserID,
		Amount:        param.Amount,
	})

	if err != nil {
		logger.Warningf("[Compensation: CompensationPaymentBalanceUpdate] error :%v", err)
		return err
	}

	return nil
}

// NewCompensationPaymentBalanceUpdate is a constructor
func NewCompensationPaymentBalanceUpdate(dep *dependency.Dependency) CompensationInterface {
	return &CompensationPaymentBalanceUpdate{
		dep: dep,
	}
}

var _ CompensationInterface = &CompensationPaymentBalanceUpdate{}
