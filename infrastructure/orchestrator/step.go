package orchestrator

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
)

// StepInterface is a method
type StepInterface interface {
	SetOrder(order *entity.Order) StepInterface
	Execute(ctx context.Context, payload *OrderPayload) (StepResult, error)
	compensate(ctx context.Context, payload *CompensationPayload) error
}

// StepResult is a result step
type StepResult map[string]any
