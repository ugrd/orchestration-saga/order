package orchestrator

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// StepPaymentBalanceUpdate is a struct
type StepPaymentBalanceUpdate struct {
	order         *entity.Order
	dep           *dependency.Dependency
	compensations []CompensationInterface
}

// SetOrder is a method
func (spb *StepPaymentBalanceUpdate) SetOrder(order *entity.Order) StepInterface {
	spb.order = order

	return spb
}

// Execute is a method
func (spb *StepPaymentBalanceUpdate) Execute(ctx context.Context, req *OrderPayload) (StepResult, error) {
	logger := log.New()

	logger.Info("[Step: StepPaymentBalanceUpdate] starting...")
	logger.Infof("[Step: StepPaymentBalanceUpdate] args: TransactionID: %v, UserID: %v, Amount: %v",
		req.TransactionID,
		req.UserID,
		spb.order.Total,
	)
	_, err := spb.dep.RPC.CreateTransaction(ctx, &payment.CreateTransactionRequest{
		TransactionId: req.TransactionID,
		UserId:        req.UserID,
		Amount:        spb.order.Total,
	})

	if err != nil {
		compPayload := CompensationPayload{
			TransportID:   req.TransportID,
			TransactionID: req.TransactionID,
			UserID:        req.UserID,
			Amount:        spb.order.Total,
		}

		for _, att := range req.OrderDetails {
			compPayload.Tickets = append(compPayload.Tickets, struct {
				ID  uint64
				Qty int
			}{ID: att.TicketID, Qty: int(att.Quantity)})
		}

		errCompensate := spb.compensate(ctx, &compPayload)
		if errCompensate != nil {
			logger.Warningf("[Step: StepPaymentBalanceUpdate] error request compensate payment: %v", err)
			return nil, ErrUnknown
		}

		logger.Warningf("[Step: StepPaymentBalanceUpdate] error request update payment: %v", err)
		grpcErr, _ := status.FromError(err)
		switch grpcErr.Code() {
		case codes.InvalidArgument:
			return nil, ErrNotEnoughBalance
		default:
			return nil, ErrUnknown
		}
	}

	return nil, nil
}

// compensate is a method
func (spb *StepPaymentBalanceUpdate) compensate(ctx context.Context, payload *CompensationPayload) error {
	for _, compensation := range spb.compensations {
		err := compensation.Execute(ctx, payload)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewStepPaymentBalanceUpdate is a constructor
func NewStepPaymentBalanceUpdate(dep *dependency.Dependency, compensations ...CompensationInterface) StepInterface {
	return &StepPaymentBalanceUpdate{
		dep:           dep,
		compensations: compensations,
	}
}

var _ StepInterface = &StepPaymentBalanceUpdate{}
