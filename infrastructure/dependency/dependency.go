package dependency

import (
	"gitlab.com/ugrd/orchestration-saga/booking/config"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
)

// Dependency collects dependencies needed by handler
type Dependency struct {
	Config     *config.Config
	Repository *registry.Repositories
	JWT        *jwt.JWT
	RPC        *rpc.RPC
}
