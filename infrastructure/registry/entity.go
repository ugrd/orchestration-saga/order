package registry

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
)

// CollectEntities is function collects entities
func CollectEntities() []contract.Entity {
	return []contract.Entity{
		{Entity: entity.Order{}},
		{Entity: entity.OrderDetail{}},
		{Entity: entity.SagaStep{}},
		{Entity: entity.SagaCompensationStep{}},
	}
}

// CollectTables is function collects entity names
func CollectTables() []contract.Table {
	var order entity.Order
	var orderDetail entity.OrderDetail
	var sagaStep entity.SagaStep
	var sagaCompensation entity.SagaCompensationStep

	return []contract.Table{
		{Name: order.TableName()},
		{Name: orderDetail.TableName()},
		{Name: sagaStep.TableName()},
		{Name: sagaCompensation.TableName()},
	}
}
