package registry

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/persistence"
	"gorm.io/gorm"
)

// Repositories is a struct which collect repositories
type Repositories struct {
	DB           *gorm.DB
	OrderRepo    repository.OrderRepoInterface
	SagaStepRepo repository.SagaStepRepoInterface
}

// NewRepo is constructor
func NewRepo(db *gorm.DB) *Repositories {
	return &Repositories{
		DB:           db,
		OrderRepo:    persistence.NewOrderRepo(db),
		SagaStepRepo: persistence.NewSagaStepRepo(db),
	}
}
