package registry

import (
	"gitlab.com/ugrd/orchestration-saga/booking/domain/contract"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
)

// NewRegistry is constructor of Registry
func NewRegistry() *entity.Registry {
	var entityRegistry []contract.Entity
	var tableRegistry []contract.Table

	entityRegistry = append(entityRegistry, CollectEntities()...)
	tableRegistry = append(tableRegistry, CollectTables()...)

	return &entity.Registry{
		Entities: entityRegistry,
		Tables:   tableRegistry,
	}
}
