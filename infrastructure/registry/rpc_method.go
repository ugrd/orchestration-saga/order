package registry

import "gitlab.com/ugrd/orchestration-saga/package/grpc/auth"

// MethodOwner is a method
type MethodOwner struct {
	Owner     []string `json:"owner"`
	Protected bool     `json:"protected"`
}

// RPCMethods is a function to hold grpc service methods
// false value indicates that the method is not protected (no authorization needed)
func RPCMethods() auth.MethodTypes {
	return map[string]*auth.MethodOwner{
		"/master.account.OrderService/CreateOrder":     {Owners: []string{"USER"}, Protect: true},
		"/master.account.OrderService/GetOrderList":    {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.OrderService/GetOrderByID":    {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.OrderService/GetOrderByToken": {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
	}
}
