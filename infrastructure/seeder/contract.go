package seeder

import (
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
)

// Seed is a contract
type Seed interface {
	Seed(repo *registry.Repositories) error
}
