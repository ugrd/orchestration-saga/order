package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/booking/pkg/common"
	"gorm.io/gorm"
)

// OrderRepo is a struct
type OrderRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r OrderRepo) Create(ctx context.Context, order *entity.Order) error {
	return r.db.WithContext(ctx).Create(order).Error
}

// Update is a method to update existing role
func (r OrderRepo) Update(ctx context.Context, order *entity.Order, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(order).Error
}

// Delete is a method to remove existing role
func (r OrderRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Order{}, id).Error
}

// FindByID is a method to retrieve transport by id
func (r OrderRepo) FindByID(ctx context.Context, id uint) (*entity.Order, error) {
	var order entity.Order

	err := r.db.WithContext(ctx).
		//Joins("OrderDetails").
		Where("orders.id = ?", id).
		Take(&order).Error
	if err != nil {
		return nil, err
	}

	return &order, nil
}

// FindByToken is a method to retrieve transport by slug
func (r OrderRepo) FindByToken(ctx context.Context, token string) (*entity.Order, error) {
	var order entity.Order

	err := r.db.WithContext(ctx).
		//Joins("OrderDetails").
		Where("orders.order_token = ?", token).
		Take(&order).Error
	if err != nil {
		return nil, err
	}

	return &order, nil
}

// FindAll is a method to retrieve transports
func (r OrderRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Order, error) {
	var orders []*entity.Order

	err := r.db.WithContext(ctx).
		//Joins("OrderDetails").
		Find(&orders).Error
	if err != nil {
		return nil, err
	}

	return orders, nil
}

// NewOrderRepo is a constructor
func NewOrderRepo(db *gorm.DB) *OrderRepo {
	return &OrderRepo{db: db}
}

var _ repository.OrderRepoInterface = &OrderRepo{}
