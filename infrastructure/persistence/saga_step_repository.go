package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/booking/pkg/common"
	"gorm.io/gorm"
)

// SagaStepRepo is a struct
type SagaStepRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r *SagaStepRepo) Create(ctx context.Context, step *entity.SagaStep) error {
	return r.db.WithContext(ctx).Create(step).Error
}

// Update is a method to update existing role
func (r *SagaStepRepo) Update(ctx context.Context, step *entity.SagaStep, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(step).Error
}

// Delete is a method to remove existing role
func (r *SagaStepRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Order{}, id).Error
}

// FindByID is a method to retrieve transport by id
func (r *SagaStepRepo) FindByID(ctx context.Context, id uint) (*entity.SagaStep, error) {
	var step entity.SagaStep

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&step).Error
	if err != nil {
		return nil, err
	}

	return &step, nil
}

// FindByOrderID is a method to retrieve transport by slug
func (r *SagaStepRepo) FindByOrderID(ctx context.Context, orderID uint) (*entity.SagaStep, error) {
	var step entity.SagaStep

	err := r.db.WithContext(ctx).
		Where("order_id = ?", orderID).
		Take(&step).Error
	if err != nil {
		return nil, err
	}

	return &step, nil
}

// FindAll is a method to retrieve transports
func (r *SagaStepRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.SagaStep, error) {
	var sagaSteps []*entity.SagaStep

	err := r.db.WithContext(ctx).
		Find(&sagaSteps).Error
	if err != nil {
		return nil, err
	}

	return sagaSteps, nil
}

// NewSagaStepRepo is a constructor
func NewSagaStepRepo(db *gorm.DB) *SagaStepRepo {
	return &SagaStepRepo{db: db}
}

var _ repository.SagaStepRepoInterface = &SagaStepRepo{}
