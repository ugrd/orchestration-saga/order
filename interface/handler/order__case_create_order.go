package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/google/uuid"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/orchestrator"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"strings"
)

// CreateOrder is a method
func (hdl *Handler) CreateOrder(ctx context.Context, req *booking.CreateOrderRequest) (*booking.Order, error) {
	logger := log.New()

	logger.Info("[CreateOrder] starting....")

	var orDetailsPayload []*CreateOrderDetailPayload
	for _, rd := range req.OrderDetails {
		orDetailsPayload = append(orDetailsPayload, &CreateOrderDetailPayload{
			TicketID:    rd.GetTicketId(),
			PassengerID: rd.GetPassengerId(),
			Quantity:    rd.GetQuantity(),
		})
	}

	payload := &CreateOrderPayload{
		TransportID:  req.GetTransportId(),
		OrderDetails: orDetailsPayload,
	}

	logger.Infof("[CreateOrder] args: TransportID: %d, OderDetails: %v", req.TransportId, req.OrderDetails)

	if err := payload.Validate(); err != nil {
		logger.Warningf("error validate payload %v", err)
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	dep := hdl.Dependency
	transactionID := genToken()

	orchestra := orchestrator.New(hdl.Dependency)
	orchestra, err := orchestra.Prepare(ctx, req.GetTransportId())
	if err != nil {
		logger.Warningf("[CreateOrder] error prepare orchestrator %v", err)
		switch err {
		case orchestrator.ErrInvalidTransport:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.InvalidArgument).
				WithMsg(fmt.Sprintf("Invalid Transport ID")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(fmt.Sprintf("Invalid account")).
				Err()
		}
	}

	order := entity.Order{
		TransportID: uint(req.GetTransportId()),
		OrderToken:  transactionID,
		UserID:      uint(orchestra.User.ID),
		Total:       0,
		Status:      entity.StatusPending,
	}

	err = hdl.Dependency.Repository.OrderRepo.Create(ctx, &order)
	if err != nil {
		logger.Warningf("[CreateOrder] error create order :%v", err)
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	orchestra = orchestra.SetOrder(&order)
	orchestra = orchestra.SetSteps(
		orchestrator.NewStepCatalogTicketUpdate(dep,
			orchestrator.NewCompensationCatalogTicketUpdate(dep),
		),
		orchestrator.NewStepPaymentBalanceUpdate(dep,
			orchestrator.NewCompensationCatalogTicketUpdate(dep),
		),
		orchestrator.NewStepSeatsUpdate(dep,
			orchestrator.NewCompensationCatalogTicketUpdate(dep),
			orchestrator.NewCompensationPaymentBalanceUpdate(dep),
		),
	)

	orcPayload := &orchestrator.OrderPayload{
		TransactionID: transactionID,
		UserID:        orchestra.User.ID,
		TransportID:   req.GetTransportId(),
	}

	for _, od := range req.OrderDetails {
		orcPayload.OrderDetails = append(orcPayload.OrderDetails, &orchestrator.OrderDetail{
			TicketID:    od.TicketId,
			PassengerID: od.PassengerId,
			Quantity:    od.Quantity,
		})
	}

	err = orchestra.Handle(ctx, orcPayload)
	if err != nil {
		logger.Warningf("[CreateOrder] error handling orchestration: %v", err)
		_ = hdl.Dependency.Repository.OrderRepo.Update(ctx, &entity.Order{
			Status: entity.StatusFailed,
		}, order.ID)

		switch err {
		case orchestrator.ErrNotEnoughBalance:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.InvalidArgument).
				WithMsg(fmt.Sprintf("Insufficient balance.")).
				Err()
		default:
			logger.Warningf("[CreateOrder] error update order to failed: %v", err)
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(fmt.Sprintf("Sorry! currently unable to process your request")).
				Err()
		}
	}

	logger.Infof("[CreateOrder] handled orchestration successfully.")

	resp, _ := hdl.Dependency.Repository.OrderRepo.FindByID(ctx, order.ID)

	return &booking.Order{
		Id:          uint64(resp.ID),
		TransportId: uint64(resp.TransportID),
		UserId:      uint64(resp.UserID),
		OrderToken:  resp.OrderToken,
		Total:       resp.Total,
		Status:      resp.Status,
		CreatedAt:   timestamppb.New(resp.CreatedAt),
		UpdatedAt:   timestamppb.New(resp.UpdatedAt),
	}, nil
}

// CreateOrderPayload is a type
type CreateOrderPayload struct {
	TransportID  uint64 `json:"transport_id"`
	OrderDetails []*CreateOrderDetailPayload
}

// CreateOrderDetailPayload is a type
type CreateOrderDetailPayload struct {
	TicketID    uint64 `json:"ticket_id"`
	PassengerID uint64 `json:"passenger_id"`
	Quantity    uint32 `json:"quantity"`
}

// Validate is a method
func (p *CreateOrderPayload) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.TransportID, validation.Required),
		validation.Field(&p.OrderDetails, validation.Required),
	)
}

// Validate is a method
func (d *CreateOrderDetailPayload) Validate() error {
	return validation.ValidateStruct(d,
		validation.Field(&d.TicketID, validation.Required),
		validation.Field(&d.PassengerID, validation.Required),
		validation.Field(&d.Quantity, validation.Required),
	)
}

// genToken is a function
func genToken() string {
	return strings.ReplaceAll(uuid.NewString(), "-", "")
}
