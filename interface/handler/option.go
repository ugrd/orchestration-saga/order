package handler

import (
	"gitlab.com/ugrd/orchestration-saga/booking/config"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
)

// Option is a type of handler option
type Option func(*Handler)

// WithConfig is a function option
func WithConfig(c *config.Config) Option {
	return func(handler *Handler) {
		handler.Dependency.Config = c
	}
}

// WithRepo is a function option
func WithRepo(r *registry.Repositories) Option {
	return func(handler *Handler) {
		handler.Dependency.Repository = r
	}
}

// WithJWT is a function option
func WithJWT(j *jwt.JWT) Option {
	return func(handler *Handler) {
		handler.Dependency.JWT = j
	}
}

// WithGRPCClient is a function option
func WithGRPCClient(client *rpc.RPC) Option {
	return func(handler *Handler) {
		handler.Dependency.RPC = client
	}
}
