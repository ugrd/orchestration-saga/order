package handler

import (
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/dependency"
	"gitlab.com/ugrd/orchestration-saga/booking/interface/handler/core"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
)

// Interface is an interface
type Interface interface {
	booking.OrderServiceServer
}

// Handler is struct
type Handler struct {
	*dependency.Dependency
	*core.GRPCService
}

// New is a constructor
func New(opts ...Option) *Handler {
	handler := &Handler{
		Dependency: &dependency.Dependency{},
	}

	for _, opt := range opts {
		opt(handler)
	}

	return handler
}

var _ Interface = &Handler{}
