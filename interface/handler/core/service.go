package core

import "gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"

// GRPCService collects grpc service server
type GRPCService struct {
	booking.UnimplementedOrderServiceServer
}
