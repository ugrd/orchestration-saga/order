package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/booking/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetOrderList is a method
func (hdl *Handler) GetOrderList(ctx context.Context, _ *booking.OrderFilterQuery) (*booking.OrderList, error) {
	logger := log.New()

	logger.Info("[CreateOrder] starting....")

	var orderList []*booking.Order
	rows, err := hdl.Dependency.Repository.OrderRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		logger.Warningf("[CreateOrder] error create order :%v", err)
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		orderList = append(orderList, &booking.Order{
			Id:          uint64(row.ID),
			TransportId: uint64(row.TransportID),
			UserId:      uint64(row.UserID),
			OrderToken:  row.OrderToken,
			Total:       row.Total,
			Status:      row.Status,
			CreatedAt:   timestamppb.New(row.CreatedAt),
			UpdatedAt:   timestamppb.New(row.UpdatedAt),
		})
	}

	return &booking.OrderList{
		Orders: orderList,
	}, nil
}
