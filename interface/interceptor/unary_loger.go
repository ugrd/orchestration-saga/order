package interceptor

import (
	"context"
	"google.golang.org/grpc"
	"log"
)

// UnaryLoggerServerInterceptor is a function for logger server unary interceptor
func (inc *Interceptor) UnaryLoggerServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		log.Printf("--> Unary interceptor: [%s]", info.FullMethod)

		return handler(ctx, req)
	}
}
