package interceptor

import (
	"gitlab.com/ugrd/orchestration-saga/booking/config"
	"gitlab.com/ugrd/orchestration-saga/booking/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
)

// Interceptor is a struct
type Interceptor struct {
	config *config.Config
	repo   *registry.Repositories
	jwt    *jwt.JWT
}

// New is a constructor
func New(conf *config.Config, repo *registry.Repositories, jwt *jwt.JWT) *Interceptor {
	return &Interceptor{
		config: conf,
		repo:   repo,
		jwt:    jwt,
	}
}
