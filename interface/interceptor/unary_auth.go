package interceptor

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/booking/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/grpc/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

// UnaryAuthServerInterceptor is a function to handle authorization
func (inc *Interceptor) UnaryAuthServerInterceptor(methods auth.MethodTypes) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		mtd := auth.NewMethodRPC(methods).
			SetMethod(info.FullMethod)

		if !mtd.ValidMethod() {
			return handler(ctx, req)
		}

		if !mtd.IsProtected() {
			return handler(ctx, req)
		}

		token, errCtx := contextParser(ctx)
		if errCtx != nil {
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(errCtx.Error()).
				Err()
		}

		tkn := token.(string)
		rsp, err := inc.jwt.Verify(tkn)

		if err != nil {
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(err.Error()).
				Err()
		}

		r := rsp.(map[string]interface{})

		mtd.SetOwner(r["access"].(string))
		if !mtd.Valid() {
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(fmt.Sprintf("Unauthenticated")).
				Err()
		}

		ctx = attachXCode(ctx, r[entity.XCodeKey].(string))
		ctx = attachXID(ctx, r[entity.XID].(string))

		return handler(ctx, req)
	}
}

// attachXCode is a function
func attachXCode(ctx context.Context, val string) context.Context {
	return context.WithValue(ctx, fmt.Sprintf(entity.XCodeKey), val)
}

// attachXID is a function
func attachXID(ctx context.Context, val string) context.Context {
	return context.WithValue(ctx, fmt.Sprintf(entity.XID), val)
}

// contextParser is a method to extract token from context
func contextParser(ctx context.Context) (interface{}, error) {
	m, valid := metadata.FromIncomingContext(ctx)
	if !valid {
		return nil, fmt.Errorf("no metadata provided")
	}

	arr, ok := m["authorization"]
	if !ok {
		return nil, fmt.Errorf("no token provided")
	}

	// arr[0] get token
	return arr[0], nil
}
